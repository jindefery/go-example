FROM golang:1.16.4 AS builder

RUN mkdir /root/.ssh/

COPY SSH_PRIVATE_KEY /root/.ssh/id_rsa

RUN rm -f SSH_PRIVATE_KEY

RUN chmod 600 /root/.ssh/id_rsa

RUN ssh-keyscan gitlab.bitkub.io >> /root/.ssh/known_hosts

RUN git config --global url."git@gitlab.bitkub.io:".insteadOf "http://gitlab.bitkub.io/"

WORKDIR /app

COPY ./src /app

RUN go env -w GOPRIVATE=gitlab.bitkub.io/*

RUN CGO_ENABLED=0 go build .

RUN rm /root/.ssh/id_rsa

FROM alpine:3.11.3

WORKDIR /app

COPY --from=builder /app/ /app

EXPOSE 8000

ENTRYPOINT ["./go-gin-example"]
