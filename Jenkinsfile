@Library('test-library') _

def imageEndpoint = ""
 if(env.BRANCH_NAME == 'dev') {
   echo "This is ${BRANCH_NAME}"
   imageEndpoint = "harbor.mark.opsta.in.th"
   echo "${env.COMMIT_ID}"
 }



pipeline {
  agent {
    kubernetes {
      yaml '''
apiVersion: v1
kind: Pod
spec:
  securityContext:
    runAsUser: 0
  volumes:
  - name: temp-data
    emptyDir: {}
  - name: jenkins-tools
    persistentVolumeClaim:
      claimName: jenkins-share
  containers:
  - name: jnlp
    image: jenkins/inbound-agent:4.3-4-alpine
    args: ['\$(JENKINS_SECRET)', '\$(JENKINS_NAME)']
    volumeMounts:
    - name: temp-data
      mountPath: /usr/local/go
    - name: jenkins-tools
      mountPath: /home/jenkins/agent/tools
  - name: docker
    image: docker:19.03.13-dind
    command:
    - dockerd
    - --host=unix:///var/run/docker.sock
    - --host=tcp://0.0.0.0:2375
    - --storage-driver=overlay2
    tty: true
    securityContext:
      privileged: true
  - name: golang
    image: golang:1.16.3-alpine
    command:
    - cat
    tty: true
    volumeMounts:
    - name: temp-data
      mountPath: /usr/local/share-data
  - name: argocd
    image: argoproj/argocd:v1.7.14
    command:
    - cat
    tty: true
    env:
    - name: ARGOCD_SERVER
      value: argocd-server.argocd.svc.cluster.mark.opsta.in.th
  - name: zap
    image: owasp/zap2docker-live
    command:
    - cat
    tty: true
    volumeMounts:
    - name: temp-data
      mountPath: /zap/wrk
'''
    }
  }

  
  environment {
    ENV_NAME = "${BRANCH_NAME}"
    // ENV_NAME = sh(script:'echo ${BRANCH_NAME} | sed s/^v//g', returnStdout: true).trim()
    COMMIT_ID = sh(returnStdout: true, script: 'git rev-parse HEAD')
    GO114MODULE = 'on'
    CGO_ENABLED = 0
  }

  parameters {
    gitParameter  name: 'TAG',
                  selectedValue: 'TOP',
                  sortMode: 'DESCENDING_SMART',
                  tagFilter: 'v*',
                  type: 'PT_TAG'
  }

  stages {

    // stage('Go Unit Testing') {
    //   when { 
    //     anyOf {
    //       branch "dev"
    //       branch "sit" 
    //       branch "staging"
    //       tag "v*"
    //     }
    //   }
    //   steps {
    //     container('golang') {
    //       script {
    //         sh 'cd src && go test .'
    //         // copy golang executor for Dependency Check
    //         sh 'cp -r /usr/local/go/* /usr/local/share-data'
    //       }
    //     }
    //   }
    // }

    // stage('OWASP Dependency Check') {
    //   when { 
    //     anyOf {
    //       branch "dev"
    //       branch "sit" 
    //       branch "staging"
    //       tag "v*"
    //     }
    //   }
    //   steps {
    //     container('jnlp') {
    //       script {
    //         dependencyCheck additionalArguments: '--scan src/go.mod -f ALL --enableExperimental --go /usr/local/go/bin/go', odcInstallation: 'DependencyCheck'
    //         dependencyCheckPublisher pattern: 'dependency-check-report.xml'
    //       }
    //     }
    //   }
    // }

    // stage('SonarQube Analysis') {
    //   when { 
    //     anyOf {
    //       branch "dev"
    //       branch "sit" 
    //       branch "staging"
    //       tag "v*"
    //     }
    //   }
    //   environment {
    //     scannerHome = tool 'SonarQubeScanner'
    // }
    //   steps {
    //     container('jnlp') {
    //       script {
    //         withSonarQubeEnv('SonarQube') {
    //           sh '${scannerHome}/bin/sonar-scanner'
    //         }
    //       }
    //     }
    //   }
    // }

    // stage("Quality Gate") {
    //   when { 
    //     anyOf {
    //       branch "dev"
    //       branch "sit" 
    //       branch "staging"
    //       tag "v*"
    //     }
    //   }
    //   steps {
    //     container('jnlp') {
    //       script {
    //         timeout(time: 1, unit: 'HOURS') {
    //           waitForQualityGate abortPipeline: true
    //         }
    //       }
    //     }
    //   }
    // }

    stage('Build Docker Image and Push to Registry') {
      when { 
        anyOf {
          branch "dev"
          branch "sit" 
          branch "staging"
          tag "v*"
        }
      }
      steps {
        buildDockerImage([
          repoEndpoint: imageEndpoint,
          repoCredentials: 'mark-harbor-registry',
          imageName:'harbor.mark.opsta.in.th/jenkins/go-example-app',
          imageTag: env.BRANCH_NAME
        ])
      }
    }

    // stage('Analyze with Anchore Engine') {
    //   when { 
    //     anyOf {
    //       branch "dev"
    //       branch "sit" 
    //       branch "staging"
    //       tag "v*"
    //     }
    //   }
    //   steps {
    //     container('jnlp'){
    //       writeFile file: 'anchore_images', text: "harbor.mark.opsta.in.th/jenkins/go-example-app:${ENV_NAME}"
    //       anchore name: 'anchore_images'
    //     }
    //   }
    // }

    // stage('Deploy to Non-Production') {
    //   when { 
    //     anyOf {
    //       branch "dev"
    //       branch "sit" 
    //       branch "staging"
    //     }
    //   }
    //   steps {
    //     container('argocd') {
    //       script {
    //           withCredentials([string(credentialsId: "argocd-deploy-role", variable: 'ARGOCD_AUTH_TOKEN')]) {
    //             sh "argocd app set go-example-${ENV_NAME} --insecure -p extraEnv.commitID=${env.BUILD_ID}-${COMMIT_ID}"
    //             sh "argocd app wait go-example-${ENV_NAME} --insecure"
    //           }
    //       }
    //     }
    //   }
    // }

    // stage('Deploy to Production') {
    //   when {
    //     branch "master"
    //     expression { return params.TAG }
    //   }

    //   steps {
    //     container('argocd') {
    //       script {

    //         withCredentials([string(credentialsId: "argocd-deploy-role", variable: 'ARGOCD_AUTH_TOKEN')]) {
    //             sh "argocd app set go-example --insecure -p app.tag=${params.TAG} --revision=${params.TAG}"
    //             sh "argocd app wait go-example --insecure"
    //         }
    //       }
    //     }
    //   }
    // }

    // stage('OWASP ZAP') {
    //   when { 
    //     anyOf {
    //       branch "dev"
    //       branch "sit" 
    //       branch "staging"
    //       tag "v*"
    //     }
    //   }
    //   steps {
    //         container('zap'){
    //           sh "zap-baseline.py -t http://go-example-dev.cluster.mark.opsta.in.th -I -r zap_report.html"
    //           sh "cp /zap/wrk/zap_report.html ${env.WORKSPACE}"
    //             publishHTML([
    //               allowMissing: false, 
    //               alwaysLinkToLastBuild: false, 
    //               keepAll: false, 
    //               reportDir: "", 
    //               reportFiles: "zap_report.html", 
    //               reportName: "ZAP DAST Report", 
    //               reportTitles: ''
    //             ])
    //         }
    //   }
    // }

  }
}
